const express = require('express');				
const app = express();				
const mongoose = require('mongoose');				
const cors = require('cors');				
const config = require('./config');

app.use(express.urlencoded({extended:false}));
app.use(express.json());				
app.use(cors());


app.listen(config.port, () => {console.log(`Listening on Port: ${config.port}`)})				
app.get('/:name', (req,res)=>{res.send('Hello ' + req.params.name);				
	}) 